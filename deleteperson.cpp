#include "deleteperson.h"
#include "ui_deleteperson.h"
#include "database.h"

DeletePerson::DeletePerson(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeletePerson)
{
    ui->setupUi(this);

    QPixmap bkgnd("..//2.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);

}

DeletePerson::~DeletePerson()
{
    delete ui;
}

void DeletePerson::on_Delete_clicked()
{
    QString Id = ui->id->text();
    string id = Id.toStdString();
    DataBase test1("personal");
    test1.del(id);
    hide();
}
