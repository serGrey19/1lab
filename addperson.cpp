#include "addperson.h"
#include "ui_addperson.h"
#include "database.h"
#include "admin.h"
#include <QMessageBox>



AddPerson::AddPerson(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddPerson)
{
    ui->setupUi(this);
    QPixmap bkgnd("..//2.jpg");
    bkgnd = bkgnd.scaled(this->size(), Qt::IgnoreAspectRatio);
    QPalette palette;
    palette.setBrush(QPalette::Background, bkgnd);
    this->setPalette(palette);
}

AddPerson::~AddPerson()
{
    delete ui;
}

void AddPerson::on_Add_clicked()
{
    QString Name = ui->Name->text();
    QString Year = ui->Year->text();
    QString Prof = ui->Prof->text();
    QString Tel = ui->Tel->text();

    string name = Name.toStdString();
    string year = Year.toStdString();
    string prof = Prof.toStdString();
    string tel = Tel.toStdString();




unsigned long long int size = tel.size();
    if(size != 10)
    {
        QMessageBox::warning(0,"Warning", "Неправильно введён номер телефона!!!");
    }

    else{

    string access;


    if(ui->Worker->isChecked())
    {
        access = "worker";
    }
    else
    {
        access = "admin";
    }


   datastruct test;
    test.name = name;
    test.year = year;
    test.prof = prof;
    test.tel = tel;
    test.access = access;

    DataBase test1("personal");
    test1.fileToVector();
    test1.add(test);
    test1.swap();
    hide();
    }
   }

