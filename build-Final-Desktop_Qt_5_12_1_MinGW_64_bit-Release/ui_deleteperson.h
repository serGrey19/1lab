/********************************************************************************
** Form generated from reading UI file 'deleteperson.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DELETEPERSON_H
#define UI_DELETEPERSON_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DeletePerson
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QLineEdit *id;
    QPushButton *Delete;
    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QDialog *DeletePerson)
    {
        if (DeletePerson->objectName().isEmpty())
            DeletePerson->setObjectName(QStringLiteral("DeletePerson"));
        DeletePerson->resize(640, 480);
        DeletePerson->setMinimumSize(QSize(640, 480));
        DeletePerson->setMaximumSize(QSize(640, 480));
        gridLayout = new QGridLayout(DeletePerson);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalSpacer = new QSpacerItem(150, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(DeletePerson);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(300, 50));
        label->setMaximumSize(QSize(300, 50));
        QFont font;
        font.setPointSize(16);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        id = new QLineEdit(DeletePerson);
        id->setObjectName(QStringLiteral("id"));
        id->setMinimumSize(QSize(300, 50));
        id->setMaximumSize(QSize(300, 50));
        id->setFont(font);
        id->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(id);


        verticalLayout_2->addLayout(verticalLayout);

        Delete = new QPushButton(DeletePerson);
        Delete->setObjectName(QStringLiteral("Delete"));
        Delete->setFont(font);

        verticalLayout_2->addWidget(Delete);


        gridLayout->addLayout(verticalLayout_2, 1, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 0, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 2, 1, 1, 1);


        retranslateUi(DeletePerson);

        QMetaObject::connectSlotsByName(DeletePerson);
    } // setupUi

    void retranslateUi(QDialog *DeletePerson)
    {
        DeletePerson->setWindowTitle(QApplication::translate("DeletePerson", "\320\243\320\264\320\260\320\273\320\265\320\275\320\270\320\265", Q_NULLPTR));
        label->setText(QApplication::translate("DeletePerson", "ID \320\220\321\200\321\202\320\270\321\201\321\202\320\260", Q_NULLPTR));
        Delete->setText(QApplication::translate("DeletePerson", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DeletePerson: public Ui_DeletePerson {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DELETEPERSON_H
