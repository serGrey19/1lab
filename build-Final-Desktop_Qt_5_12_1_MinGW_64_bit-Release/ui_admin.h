/********************************************************************************
** Form generated from reading UI file 'admin.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMIN_H
#define UI_ADMIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Admin
{
public:
    QGridLayout *gridLayout;
    QComboBox *city;
    QHBoxLayout *horizontalLayout_2;
    QComboBox *room;
    QPushButton *pushButton2;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *Id;
    QLineEdit *Search_Name;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QLineEdit *Search_Year;
    QLabel *VashID;
    QPushButton *pushButton3;
    QTextEdit *Text;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButtonAdd;
    QPushButton *pushButtonEdit;
    QPushButton *pushButtonDelete;
    QPushButton *pushButton1;

    void setupUi(QDialog *Admin)
    {
        if (Admin->objectName().isEmpty())
            Admin->setObjectName(QStringLiteral("Admin"));
        Admin->setEnabled(true);
        Admin->resize(640, 480);
        Admin->setMinimumSize(QSize(640, 480));
        Admin->setMaximumSize(QSize(640, 480));
        Admin->setWindowTitle(QString::fromUtf8("\320\220\320\264\320\274\320\270\320\275"));
        Admin->setStyleSheet(QStringLiteral(""));
        gridLayout = new QGridLayout(Admin);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        city = new QComboBox(Admin);
        city->setObjectName(QStringLiteral("city"));

        gridLayout->addWidget(city, 7, 1, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));

        gridLayout->addLayout(horizontalLayout_2, 2, 1, 1, 1);

        room = new QComboBox(Admin);
        room->setObjectName(QStringLiteral("room"));

        gridLayout->addWidget(room, 7, 2, 1, 1);

        pushButton2 = new QPushButton(Admin);
        pushButton2->setObjectName(QStringLiteral("pushButton2"));
        pushButton2->setMinimumSize(QSize(370, 50));
        pushButton2->setMaximumSize(QSize(370, 60));

        gridLayout->addWidget(pushButton2, 8, 1, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        Id = new QLabel(Admin);
        Id->setObjectName(QStringLiteral("Id"));
        QFont font;
        font.setPointSize(16);
        Id->setFont(font);

        verticalLayout_2->addWidget(Id);

        Search_Name = new QLineEdit(Admin);
        Search_Name->setObjectName(QStringLiteral("Search_Name"));

        verticalLayout_2->addWidget(Search_Name);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_3 = new QLabel(Admin);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setFont(font);

        verticalLayout_4->addWidget(label_3);

        Search_Year = new QLineEdit(Admin);
        Search_Year->setObjectName(QStringLiteral("Search_Year"));

        verticalLayout_4->addWidget(Search_Year);


        horizontalLayout->addLayout(verticalLayout_4);


        gridLayout->addLayout(horizontalLayout, 0, 1, 1, 1);

        VashID = new QLabel(Admin);
        VashID->setObjectName(QStringLiteral("VashID"));
        VashID->setMinimumSize(QSize(0, 0));
        VashID->setMaximumSize(QSize(600, 30));
        VashID->setFont(font);
        VashID->setLayoutDirection(Qt::RightToLeft);

        gridLayout->addWidget(VashID, 0, 0, 1, 1);

        pushButton3 = new QPushButton(Admin);
        pushButton3->setObjectName(QStringLiteral("pushButton3"));
        pushButton3->setMinimumSize(QSize(120, 70));
        pushButton3->setMaximumSize(QSize(120, 70));
        pushButton3->setStyleSheet(QStringLiteral(""));

        gridLayout->addWidget(pushButton3, 0, 2, 1, 1);

        Text = new QTextEdit(Admin);
        Text->setObjectName(QStringLiteral("Text"));
        Text->setStyleSheet(QStringLiteral(""));

        gridLayout->addWidget(Text, 4, 1, 1, 2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        verticalLayout->setContentsMargins(-1, -1, -1, 0);
        pushButtonAdd = new QPushButton(Admin);
        pushButtonAdd->setObjectName(QStringLiteral("pushButtonAdd"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButtonAdd->sizePolicy().hasHeightForWidth());
        pushButtonAdd->setSizePolicy(sizePolicy);
        pushButtonAdd->setMinimumSize(QSize(240, 80));
        pushButtonAdd->setMaximumSize(QSize(260, 80));
        pushButtonAdd->setStyleSheet(QStringLiteral(""));

        verticalLayout->addWidget(pushButtonAdd);

        pushButtonEdit = new QPushButton(Admin);
        pushButtonEdit->setObjectName(QStringLiteral("pushButtonEdit"));
        pushButtonEdit->setMinimumSize(QSize(240, 80));
        pushButtonEdit->setMaximumSize(QSize(260, 80));

        verticalLayout->addWidget(pushButtonEdit);

        pushButtonDelete = new QPushButton(Admin);
        pushButtonDelete->setObjectName(QStringLiteral("pushButtonDelete"));
        pushButtonDelete->setMinimumSize(QSize(240, 80));
        pushButtonDelete->setMaximumSize(QSize(260, 80));

        verticalLayout->addWidget(pushButtonDelete);

        pushButton1 = new QPushButton(Admin);
        pushButton1->setObjectName(QStringLiteral("pushButton1"));
        pushButton1->setMinimumSize(QSize(240, 80));
        pushButton1->setMaximumSize(QSize(260, 80));

        verticalLayout->addWidget(pushButton1);


        gridLayout->addLayout(verticalLayout, 2, 0, 6, 1);


        retranslateUi(Admin);

        QMetaObject::connectSlotsByName(Admin);
    } // setupUi

    void retranslateUi(QDialog *Admin)
    {
        city->clear();
        city->insertItems(0, QStringList()
         << QApplication::translate("Admin", "\320\241\320\260\320\275\320\272\321\202-\320\237\320\265\321\202\320\265\321\200\320\261\321\203\321\200\320\263", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\222\320\273\320\260\320\264\320\270\320\262\320\276\321\201\321\202\320\276\320\272", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\222\320\276\320\273\320\263\320\276\320\263\321\200\320\260\320\264", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\234\320\276\321\201\320\272\320\262\320\260", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\241\320\260\321\200\320\260\321\202\320\276\320\262", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\240\320\276\321\201\321\202\320\276\320\262", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\222\320\273\320\260\320\264\320\270\320\274\320\270\321\200", Q_NULLPTR)
        );
        room->clear();
        room->insertItems(0, QStringList()
         << QApplication::translate("Admin", "\320\227\320\260\320\273 \342\204\2261", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\227\320\260\320\273 \342\204\2262", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\227\320\260\320\273 \342\204\2263", Q_NULLPTR)
         << QApplication::translate("Admin", "\320\227\320\260\320\273 \342\204\2264", Q_NULLPTR)
        );
        pushButton2->setText(QApplication::translate("Admin", "\320\236\321\202\320\274\320\265\321\202\320\270\321\202\321\214 \320\262\321\213\321\205\320\276\320\264 \320\275\320\260 \321\201\321\206\320\265\320\275\321\203", Q_NULLPTR));
        Id->setText(QApplication::translate("Admin", "\320\230\320\274\321\217", Q_NULLPTR));
        label_3->setText(QApplication::translate("Admin", "\320\223\320\276\320\264", Q_NULLPTR));
        VashID->setText(QApplication::translate("Admin", "\320\222\320\260\321\210 ID - ", Q_NULLPTR));
        pushButton3->setText(QApplication::translate("Admin", "\320\237\320\276\320\270\321\201\320\272", Q_NULLPTR));
        Text->setHtml(QApplication::translate("Admin", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:7.8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8.25pt;\"><br /></p></body></html>", Q_NULLPTR));
        pushButtonAdd->setText(QApplication::translate("Admin", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", Q_NULLPTR));
        pushButtonEdit->setText(QApplication::translate("Admin", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\321\202\321\214 \320\264\320\260\320\275\320\275\321\213\320\265", Q_NULLPTR));
        pushButtonDelete->setText(QApplication::translate("Admin", "\320\243\320\264\320\260\320\273\320\270\321\202\321\214", Q_NULLPTR));
        pushButton1->setText(QApplication::translate("Admin", "\320\222\321\213\320\262\320\276\320\264 \320\264\320\260\320\275\320\275\321\213\321\205", Q_NULLPTR));
        Q_UNUSED(Admin);
    } // retranslateUi

};

namespace Ui {
    class Admin: public Ui_Admin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMIN_H
