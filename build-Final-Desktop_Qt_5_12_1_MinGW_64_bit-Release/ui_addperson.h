/********************************************************************************
** Form generated from reading UI file 'addperson.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADDPERSON_H
#define UI_ADDPERSON_H

#include <QtCore/QLocale>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_AddPerson
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *verticalSpacer;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *Name;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *Year;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QLineEdit *Prof;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QLineEdit *Tel;
    QHBoxLayout *horizontalLayout_5;
    QSpacerItem *horizontalSpacer_3;
    QRadioButton *Admin;
    QRadioButton *Worker;
    QPushButton *Add;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QDialog *AddPerson)
    {
        if (AddPerson->objectName().isEmpty())
            AddPerson->setObjectName(QStringLiteral("AddPerson"));
        AddPerson->resize(640, 480);
        AddPerson->setMinimumSize(QSize(640, 480));
        AddPerson->setMaximumSize(QSize(640, 480));
        QFont font;
        font.setPointSize(16);
        AddPerson->setFont(font);
        gridLayout = new QGridLayout(AddPerson);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer, 0, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 1, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label = new QLabel(AddPerson);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label);

        Name = new QLineEdit(AddPerson);
        Name->setObjectName(QStringLiteral("Name"));
        Name->setEnabled(true);
        Name->setMinimumSize(QSize(300, 50));
        Name->setMaximumSize(QSize(300, 50));
        QFont font1;
        font1.setItalic(false);
        Name->setFont(font1);
        Name->setCursor(QCursor(Qt::IBeamCursor));
        Name->setTabletTracking(false);
        Name->setContextMenuPolicy(Qt::DefaultContextMenu);
        Name->setLocale(QLocale(QLocale::Russian, QLocale::Russia));
        Name->setMaxLength(50);
        Name->setEchoMode(QLineEdit::Normal);
        Name->setAlignment(Qt::AlignCenter);
        Name->setDragEnabled(false);

        horizontalLayout->addWidget(Name);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_2 = new QLabel(AddPerson);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_2);

        Year = new QLineEdit(AddPerson);
        Year->setObjectName(QStringLiteral("Year"));
        Year->setMinimumSize(QSize(300, 50));
        Year->setMaximumSize(QSize(300, 50));
        Year->setFont(font1);
        Year->setContextMenuPolicy(Qt::DefaultContextMenu);
        Year->setLocale(QLocale(QLocale::Russian, QLocale::Russia));
        Year->setMaxLength(50);
        Year->setEchoMode(QLineEdit::Normal);
        Year->setAlignment(Qt::AlignCenter);
        Year->setDragEnabled(false);

        horizontalLayout_2->addWidget(Year);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label_3 = new QLabel(AddPerson);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setAlignment(Qt::AlignCenter);

        horizontalLayout_3->addWidget(label_3);

        Prof = new QLineEdit(AddPerson);
        Prof->setObjectName(QStringLiteral("Prof"));
        Prof->setMinimumSize(QSize(300, 50));
        Prof->setMaximumSize(QSize(300, 50));
        Prof->setFont(font1);
        Prof->setContextMenuPolicy(Qt::DefaultContextMenu);
        Prof->setLocale(QLocale(QLocale::Russian, QLocale::Russia));
        Prof->setMaxLength(32767);
        Prof->setEchoMode(QLineEdit::Normal);
        Prof->setAlignment(Qt::AlignCenter);
        Prof->setDragEnabled(false);

        horizontalLayout_3->addWidget(Prof);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_4 = new QLabel(AddPerson);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setAlignment(Qt::AlignCenter);

        horizontalLayout_4->addWidget(label_4);

        Tel = new QLineEdit(AddPerson);
        Tel->setObjectName(QStringLiteral("Tel"));
        Tel->setMinimumSize(QSize(300, 50));
        Tel->setMaximumSize(QSize(300, 50));
        Tel->setFont(font1);
        Tel->setContextMenuPolicy(Qt::DefaultContextMenu);
        Tel->setLocale(QLocale(QLocale::Russian, QLocale::Russia));
        Tel->setMaxLength(50);
        Tel->setEchoMode(QLineEdit::Normal);
        Tel->setAlignment(Qt::AlignCenter);
        Tel->setDragEnabled(false);

        horizontalLayout_4->addWidget(Tel);


        verticalLayout->addLayout(horizontalLayout_4);


        verticalLayout_2->addLayout(verticalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalSpacer_3 = new QSpacerItem(20, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_5->addItem(horizontalSpacer_3);

        Admin = new QRadioButton(AddPerson);
        Admin->setObjectName(QStringLiteral("Admin"));
        Admin->setMinimumSize(QSize(147, 0));
        Admin->setMaximumSize(QSize(147, 16777215));

        horizontalLayout_5->addWidget(Admin);

        Worker = new QRadioButton(AddPerson);
        Worker->setObjectName(QStringLiteral("Worker"));
        Worker->setMinimumSize(QSize(147, 0));
        Worker->setMaximumSize(QSize(147, 16777215));
        Worker->setCheckable(true);
        Worker->setChecked(true);

        horizontalLayout_5->addWidget(Worker);


        verticalLayout_2->addLayout(horizontalLayout_5);

        Add = new QPushButton(AddPerson);
        Add->setObjectName(QStringLiteral("Add"));

        verticalLayout_2->addWidget(Add);


        gridLayout->addLayout(verticalLayout_2, 1, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 1, 2, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout->addItem(verticalSpacer_2, 2, 1, 1, 1);


        retranslateUi(AddPerson);

        QMetaObject::connectSlotsByName(AddPerson);
    } // setupUi

    void retranslateUi(QDialog *AddPerson)
    {
        AddPerson->setWindowTitle(QApplication::translate("AddPerson", "\320\224\320\276\320\261\320\260\320\262\320\273\320\265\320\275\320\270\320\265", Q_NULLPTR));
        label->setText(QApplication::translate("AddPerson", "\320\230\320\274\321\217:", Q_NULLPTR));
#ifndef QT_NO_STATUSTIP
        Name->setStatusTip(QString());
#endif // QT_NO_STATUSTIP
        Name->setText(QString());
        label_2->setText(QApplication::translate("AddPerson", "\320\223\320\276\320\264:", Q_NULLPTR));
        Year->setText(QString());
        label_3->setText(QApplication::translate("AddPerson", "\320\224\320\276\320\273\320\266\320\275\320\276\321\201\321\202\321\214:", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        Prof->setAccessibleName(QString());
#endif // QT_NO_ACCESSIBILITY
        Prof->setInputMask(QString());
        Prof->setText(QString());
        Prof->setPlaceholderText(QString());
        label_4->setText(QApplication::translate("AddPerson", "\320\242\320\265\320\273\320\265\321\204\320\276\320\275:", Q_NULLPTR));
        Tel->setText(QString());
        Admin->setText(QApplication::translate("AddPerson", "\320\220\320\264\320\274\320\270\320\275", Q_NULLPTR));
        Worker->setText(QApplication::translate("AddPerson", "\320\220\321\200\321\202\320\270\321\201\321\202", Q_NULLPTR));
        Add->setText(QApplication::translate("AddPerson", "\320\224\320\276\320\261\320\260\320\262\320\270\321\202\321\214", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AddPerson: public Ui_AddPerson {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADDPERSON_H
