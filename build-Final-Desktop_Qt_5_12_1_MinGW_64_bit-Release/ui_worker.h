/********************************************************************************
** Form generated from reading UI file 'worker.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WORKER_H
#define UI_WORKER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_worker
{
public:
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout_2;
    QComboBox *city;
    QComboBox *room;
    QPushButton *pushButton2;
    QTextEdit *Text;
    QPushButton *pushButtonEdit;
    QVBoxLayout *verticalLayout;
    QLabel *VashID;

    void setupUi(QDialog *worker)
    {
        if (worker->objectName().isEmpty())
            worker->setObjectName(QStringLiteral("worker"));
        worker->resize(640, 480);
        worker->setWindowTitle(QString::fromUtf8("\320\220\321\200\321\202\320\270\321\201\321\202"));
        gridLayout = new QGridLayout(worker);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        city = new QComboBox(worker);
        city->setObjectName(QStringLiteral("city"));

        horizontalLayout_2->addWidget(city);

        room = new QComboBox(worker);
        room->setObjectName(QStringLiteral("room"));

        horizontalLayout_2->addWidget(room);


        gridLayout->addLayout(horizontalLayout_2, 7, 0, 1, 1);

        pushButton2 = new QPushButton(worker);
        pushButton2->setObjectName(QStringLiteral("pushButton2"));
        pushButton2->setMinimumSize(QSize(260, 40));
        pushButton2->setMaximumSize(QSize(260, 80));

        gridLayout->addWidget(pushButton2, 7, 1, 1, 1);

        Text = new QTextEdit(worker);
        Text->setObjectName(QStringLiteral("Text"));
        Text->setEnabled(true);
        Text->setAcceptDrops(true);
        Text->setStyleSheet(QStringLiteral(""));

        gridLayout->addWidget(Text, 6, 0, 1, 2);

        pushButtonEdit = new QPushButton(worker);
        pushButtonEdit->setObjectName(QStringLiteral("pushButtonEdit"));
        pushButtonEdit->setMinimumSize(QSize(260, 80));
        pushButtonEdit->setMaximumSize(QSize(260, 80));
        pushButtonEdit->setLayoutDirection(Qt::LeftToRight);

        gridLayout->addWidget(pushButtonEdit, 1, 1, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetFixedSize);
        verticalLayout->setContentsMargins(-1, -1, -1, 0);
        VashID = new QLabel(worker);
        VashID->setObjectName(QStringLiteral("VashID"));
        VashID->setMinimumSize(QSize(0, 0));
        VashID->setMaximumSize(QSize(150, 30));
        QFont font;
        font.setPointSize(16);
        VashID->setFont(font);

        verticalLayout->addWidget(VashID);


        gridLayout->addLayout(verticalLayout, 1, 0, 1, 1);


        retranslateUi(worker);

        QMetaObject::connectSlotsByName(worker);
    } // setupUi

    void retranslateUi(QDialog *worker)
    {
        city->clear();
        city->insertItems(0, QStringList()
         << QApplication::translate("worker", "\320\232\321\200\320\260\321\201\320\275\320\276\320\264\320\260\321\200\321\201\320\272\320\270\320\271 \320\272\321\200\320\260\320\271", Q_NULLPTR)
         << QApplication::translate("worker", "\320\242\321\203\320\273\321\214\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", Q_NULLPTR)
         << QApplication::translate("worker", "\320\222\320\276\320\273\320\276\320\263\320\276\320\264\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", Q_NULLPTR)
         << QApplication::translate("worker", "\320\234\320\276\321\201\320\272\320\262\320\260", Q_NULLPTR)
         << QApplication::translate("worker", "\320\247\321\203\320\262\320\260\321\210\320\270\321\217", Q_NULLPTR)
         << QApplication::translate("worker", "\320\240\320\276\321\201\321\202\320\276\320\262\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", Q_NULLPTR)
         << QApplication::translate("worker", "\320\222\320\273\320\260\320\264\320\270\320\274\320\270\321\200\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", Q_NULLPTR)
         << QApplication::translate("worker", "\320\221\321\200\321\217\320\275\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", Q_NULLPTR)
         << QApplication::translate("worker", "\320\241\320\260\320\274\320\260\321\200\321\201\320\272\320\260\321\217 \320\276\320\261\320\273\320\260\321\201\321\202\321\214", Q_NULLPTR)
        );
        room->clear();
        room->insertItems(0, QStringList()
         << QApplication::translate("worker", "\320\227\320\260\320\273\342\204\2261", Q_NULLPTR)
         << QApplication::translate("worker", "\320\227\320\260\320\273\342\204\2262", Q_NULLPTR)
         << QApplication::translate("worker", "\320\227\320\260\320\273\342\204\2263", Q_NULLPTR)
         << QApplication::translate("worker", "\320\227\320\260\320\273\342\204\2264", Q_NULLPTR)
        );
        pushButton2->setText(QApplication::translate("worker", "\320\236\321\202\320\274\320\265\321\202\320\270\321\202\321\214 \320\262\321\213\321\205\320\276\320\264 \320\275\320\260 \321\201\321\206\320\265\320\275\321\203", Q_NULLPTR));
        Text->setHtml(QApplication::translate("worker", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:7.8pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8.25pt;\"><br /></p></body></html>", Q_NULLPTR));
        pushButtonEdit->setText(QApplication::translate("worker", "\320\240\320\265\320\264\320\260\320\272\321\202\320\270\321\200\320\276\320\262\320\260\320\275\320\270\320\265 \320\264\320\260\320\275\320\275\321\213\321\205", Q_NULLPTR));
        VashID->setText(QApplication::translate("worker", "\320\222\320\260\321\210 ID - ", Q_NULLPTR));
        Q_UNUSED(worker);
    } // retranslateUi

};

namespace Ui {
    class worker: public Ui_worker {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WORKER_H
